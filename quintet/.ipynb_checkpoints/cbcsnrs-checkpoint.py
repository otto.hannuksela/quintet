import numpy as np
from bilby.core import utils
from pycbc.detector import Detector
import bilby
# Define a class for computing the inner products:
class CBCSNRsBase():
    def __init__(self, mass_1, mass_2, sampling_frequency=4096, waveform_arguments=  dict(waveform_approximant = "IMRPhenomXPHM", reference_frequency = 50., minimum_frequency = 40), list_of_detectors=["H1", "L1", "V1"]):
        self.mass_1 = mass_1
        self.mass_2 = mass_2
        nsamples = len(mass_1) # Set to be the same size as the input vector
        # Fix the other parameters
        luminosity_distance = 100*np.ones(nsamples)
        theta_jn = 0*np.ones(nsamples)
        a_1, a_2, tilt_1, tilt_2, phi_12, phi_jl = 0*np.ones(nsamples),0*np.ones(nsamples),0*np.ones(nsamples),0*np.ones(nsamples),0*np.ones(nsamples),0*np.ones(nsamples)
        ra, dec, psi, phase, geocent_time = 0*np.ones(nsamples), 0*np.ones(nsamples), 0*np.ones(nsamples), 0*np.ones(nsamples), 1000*np.ones(nsamples)
        # Initialize PSDs and IFOs
        self.__initialize_psds()
        self.__initialize_ifos(list_of_detectors)
        self.list_of_detectors = list_of_detectors
        self.waveform_arguments = waveform_arguments
        self.sampling_frequency = sampling_frequency
        # Save the inner products
        hp_inner_hp = dict()
        hp_inner_hc = dict()
        hc_inner_hc = dict()
        for ifo in list_of_detectors:
            hp_inner_hp[ifo] = []
            hp_inner_hc[ifo] = []
            hc_inner_hc[ifo] = []
            for i in range(nsamples):
                hp_inner_hp_i, hp_inner_hc_i, hc_inner_hc_i = self.inner_products(i, mass_1, mass_2, a_1, a_2, tilt_1, tilt_2, phi_12, phi_jl, luminosity_distance, ra, dec, psi, theta_jn, phase, geocent_time)
                hp_inner_hp[ifo].append(hp_inner_hp_i[ifo])
                hp_inner_hc[ifo].append(hp_inner_hc_i[ifo])
                hc_inner_hc[ifo].append(hc_inner_hc_i[ifo])
            hp_inner_hp[ifo] = np.array(hp_inner_hp[ifo]) 
            hp_inner_hc[ifo] = np.array(hp_inner_hc[ifo]) 
            hc_inner_hc[ifo] = np.array(hc_inner_hc[ifo]) 
        self.hp_inner_hp = hp_inner_hp
        self.hp_inner_hc = hp_inner_hc
        self.hc_inner_hc = hc_inner_hc
        self.nsamples = nsamples
        return

    def inner_product_simple(self, parameters):
        # now we can setup the waveform generator to 
        # make the two polarizations
        # first need to have an approximate signal duration
        approx_duration = bilby.gw.utils.calculate_time_to_merger(self.waveform_arguments['minimum_frequency'],
                                                                  parameters['mass_1'],
                                                                  parameters['mass_2'],
                                                                  safety = 1.2)
        duration = np.ceil(approx_duration + 4.)
        utils.logger.disabled = True # Stop debug messages
        waveform_generator = bilby.gw.WaveformGenerator(duration = duration, sampling_frequency = self.sampling_frequency, frequency_domain_source_model = bilby.gw.source.lal_binary_black_hole, waveform_arguments = self.waveform_arguments)
        utils.logger.disabled = False # Enable debug messages (this allows us to avoid spam)
        polas = waveform_generator.frequency_domain_strain(parameters = parameters)
        done_ifos = []
        done_psds = []
        hp_inner_hp = dict()
        hp_inner_hc = dict()
        hc_inner_hc = dict()
        for ifo in self.list_of_detectors:
            if self.psds[ifo] in done_psds:
                # already computed the inner product for 
                # the corresponsing PSD
                seen_ifo = done_ifos[done_psds.index(self.psds[ifo])]
                hp_inner_hp[ifo] = hp_inner_hp[seen_ifo]
                hp_inner_hc[ifo] = hp_inner_hc[seen_ifo]
                hc_inner_hc[ifo] = hc_inner_hc[seen_ifo]
            else:
                # need to compute the inner product for
                # the firs time
                p_array = self.psds_arrays[ifo].get_power_spectral_density_array(waveform_generator.frequency_array)
                hp_inner_hp[ifo] = bilby.gw.utils.noise_weighted_inner_product(polas['plus'],
                                                                               polas['plus'],
                                                                               p_array,
                                                                               waveform_generator.duration)
                hp_inner_hc[ifo] = bilby.gw.utils.noise_weighted_inner_product(polas['plus'],
                                                                               polas['cross'],
                                                                               p_array,
                                                                               waveform_generator.duration)
                hc_inner_hc[ifo] = bilby.gw.utils.noise_weighted_inner_product(polas['cross'],
                                                                               polas['cross'],
                                                                               p_array,
                                                                               waveform_generator.duration)
                done_ifos.append(ifo)
                done_psds.append(self.psds[ifo])
        return hp_inner_hp, hp_inner_hc, hc_inner_hc


    def inner_products(self, i, mass_1, mass_2, a_1, a_2, tilt_1, tilt_2, phi_12, phi_jl, luminosity_distance, ra, dec, psi, theta_jn, phase, geocent_time):
        parameters = dict(mass_1 = mass_1[i], mass_2 = mass_2[i],
                          a_1 = a_1[i], a_2 = a_2[i],
                          tilt_1 = tilt_1[i], tilt_2 = tilt_2[i],
                          phi_12 = phi_12[i], phi_jl = phi_jl[i],
                          luminosity_distance = luminosity_distance[i],
                          ra = ra[i],
                          dec = dec[i],
                          psi = psi[i],
                          theta_jn = theta_jn[i],
                          phase = phase[i],
                          geocent_time = geocent_time[i])
        hp_inner_hp, hp_inner_hc, hc_inner_hc = self.inner_product_simple(parameters)
        return hp_inner_hp, hp_inner_hc, hc_inner_hc
    
    def __initialize_psds(self):
        psds = dict()
        psds['L1'] = 'aLIGO_O4_high_asd.txt'
        psds['H1'] = 'aLIGO_O4_high_asd.txt'
        psds['V1'] = 'AdV_asd.txt'
        psds['K1'] = 'KAGRA_design_asd.txt'
        psds['a_1'] = 'aLIGO_O4_high_asd.txt'
        psd_file=False
        # load psds once and for all as useful objects
        psds_arrays = dict()
        if psd_file:
            for key in psds:
                psds_arrays[key] = bilby.gw.detector.PowerSpectralDensity(psd_file = psds[key])
        else:
            for key in psds:
                psds_arrays[key] = bilby.gw.detector.PowerSpectralDensity(asd_file = psds[key])
        self.psds = psds
        self.psds_arrays = psds_arrays
        return psds, psds_arrays
    
    def __initialize_ifos(self, list_of_detectors):
        # make the ifos as bilby ifo's object to be used later
        ifos_objects = dict()
        for key in list_of_detectors:
            ifos_objects[key] = bilby.gw.detector.networks.get_empty_interferometer(key)
        self.ifos_objects = ifos_objects
        # Create fast ifo
        fast_ifo_objects = dict()
        for detector in list_of_detectors:
            fast_ifo_object = Detector(detector) # create pycbc detector
            fast_ifo_objects[detector] = fast_ifo_object
        self.fast_ifo_objects = fast_ifo_objects
        return ifos_objects, fast_ifo_objects

class BNSSNRInterpolator(CBCSNRsBase):
    def __init__(self, total_mass_min, total_mass_max, sampling_frequency=4096, waveform_arguments= dict(waveform_approximant = "TaylorF2", reference_frequency = 50., minimum_frequency = 10), list_of_detectors=["L1"]):
        # Set masses to be the same for now:
        nsamples = 100
        total_mass = np.geomspace(total_mass_min,  total_mass_max, nsamples)
        mass_1 = total_mass/2 # Equal mass in preliminary computation
        mass_2 = total_mass/2 # Equal mass in preliminary computation
        # Compute the inner products for each mass
        super().__init__(mass_1, mass_2, sampling_frequency, waveform_arguments, list_of_detectors)
        # Save values
        self.total_mass_min = total_mass_min
        self.total_mass_max = total_mass_max
        self.total_mass_table = total_mass
    def interpolate_inner_products_idx(self, total_mass):
        # Zeroth order approximation: Just return the inner product that is nearest to the correct mass value
        idx = np.searchsorted(self.total_mass_table, total_mass)
        return idx
    def single_detector_optimal_snr(self, mass_1, mass_2, luminosity_distance, theta_jn, ra, dec, psi, phase, geocent_time, ifo='L1'):
        ''' Single-detector optimal snr
        '''
        total_mass = mass_1+mass_2
        mu = mass_1*mass_2/total_mass
        chirp_mass = mu**(3./5.)/total_mass**(2/5)
        chirp_mass_unscaled = total_mass**(1./5.)/(2.*2**(1./5.)) # Assuming q=1
        iota = theta_jn
        indices = self.interpolate_inner_products_idx(total_mass) # Get the indices for the inner products
        Fp, Fc = self.fast_ifo_objects[ifo].antenna_pattern(ra, dec, psi, geocent_time)
        hp_inner_hp, hc_inner_hc = self.hp_inner_hp[ifo][indices], self.hc_inner_hc[ifo][indices]
        Deff1 = luminosity_distance/np.sqrt(Fp**2*((1+np.cos(iota)**2)/2)**2+Fc**2*np.cos(iota)**2 )
        Deff2 = 100/np.sqrt(Fp**2*((1+np.cos(0)**2)/2)**2+Fc**2*np.cos(0)**2 )
        A1 = chirp_mass**(5./6.)
        A2 = chirp_mass_unscaled**(5./6.)
        optimal_snr_squared_unscaled = Fp**2*hp_inner_hp + Fc**2*hc_inner_hc
        # Rescale:
        return (Deff2/Deff1)*(A1/A2)*np.sqrt(np.real(optimal_snr_squared_unscaled))
    def evaluate(self, mass_1, mass_2, luminosity_distance, theta_jn, ra, dec, psi, phase, geocent_time):
        network_optimal_snr_squared = 0
        for ifo in self.list_of_detectors:
            single_detector_optimal_snr = self.single_detector_optimal_snr(mass_1, mass_2, luminosity_distance, theta_jn, ra, dec, psi, phase, geocent_time, ifo)
            # Add in quadrature
            network_optimal_snr_squared = network_optimal_snr_squared + single_detector_optimal_snr**2
        # Return the  network snr
        network_optimal_snr = np.sqrt(network_optimal_snr_squared)
        return network_optimal_snr
    def __call__(self, mass_1, mass_2, luminosity_distance, theta_jn, ra, dec, psi, phase, geocent_time):
        return self.evaluate(mass_1, mass_2, luminosity_distance, theta_jn, ra, dec, psi, phase, geocent_time)

# Function for computing bilby SNRs
def compute_bilby_snr(mass_1, mass_2, a_1, a_2, tilt_1, tilt_2, phi_12, phi_jl, luminosity_distance, theta_jn, psi, phase, geocent_time, ra, dec, duration=4.0, sampling_frequency=4096, waveform_arguments=  dict(waveform_approximant = "IMRPhenomXPHM", reference_frequency = 50., minimum_frequency = 40), list_of_detectors=["L1"]):
    # Set the duration and sampling frequency of the data segment that we're
    # going to inject the signal into
    # Specify the output directory and the name of the simulation.
    bilby.core.utils.logger.disabled = True
    # Set up a random seed for result reproducibility. 
    np.random.seed(88170235)
    # We are going to inject a binary black hole waveform.  We first establish a
    # dictionary of parameters that includes all of the different waveform
    # parameters, including masses of the two black holes (mass_1, mass_2),
    # spins of both black holes (a, tilt, phi), etc.
    injection_parameters = dict(
        mass_1=mass_1,
        mass_2=mass_2,
        a_1=a_1,
        a_2=a_2,
        tilt_1=tilt_1,
        tilt_2=tilt_2,
        phi_12=phi_12,
        phi_jl=phi_jl,
        luminosity_distance=luminosity_distance,
        theta_jn=theta_jn,
        psi=psi,
        phase=phase,
        geocent_time=geocent_time,
        ra=ra,
        dec=dec
    )
    # Create the waveform_generator using a LAL BinaryBlackHole source function
    # the generator will convert all the parameters
    waveform_generator = bilby.gw.WaveformGenerator(
        duration=duration,
        sampling_frequency=sampling_frequency,
        frequency_domain_source_model=bilby.gw.source.lal_binary_black_hole,
        parameter_conversion=bilby.gw.conversion.convert_to_lal_binary_black_hole_parameters,
        waveform_arguments=waveform_arguments,
    )
    # Set up interferometers.  In this case we'll use two interferometers
    # (LIGO-Hanford (H1), LIGO-Livingston (L1). These default to their design
    # sensitivity
    ifos = bilby.gw.detector.InterferometerList(list_of_detectors)
    ifos.set_strain_data_from_power_spectral_densities(
        sampling_frequency=sampling_frequency,
        duration=duration,
        start_time=injection_parameters["geocent_time"] - 2,
    )
    ifos.inject_signal(
        waveform_generator=waveform_generator, parameters=injection_parameters
    );
    network_optimal_snr_squared = 0
    for i in range(len(ifos)):
        single_detector_optimal_snr = ifos[i].meta_data['optimal_SNR']
        # Sum in quadrature
        network_optimal_snr_squared = network_optimal_snr_squared + single_detector_optimal_snr**2
    network_optimal_snr = np.sqrt(network_optimal_snr_squared)
    return network_optimal_snr



