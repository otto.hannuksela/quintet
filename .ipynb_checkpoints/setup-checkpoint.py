#!/usr/bin/env python
from setuptools import setup, find_packages
setup(name='quintet',
      version='0.2',
      description='Fast SNR interpolator',
      author='Hemantakumar, Otto',
      license="MIT",
      author_email='hemantaphurailatpam@gmail.com',
      url='https://git.ligo.org/otto.hannuksela/quintet',
      packages=find_packages(),
      install_requires=[
        "setuptools>=61.1.0",
        "bilby>=1.0.2",
        "pycbc>=2.0.4",
        "scipy>=1.9.0",
      ]
     )
